<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Productos;
use app\models\Categorias;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
        return $this->render('index');
    }

    public function actionOfertas()
    {
        $o= Productos::find()
                ->where(['oferta'=> True])
                ->all();
        
        return $this->render('ofertas',[
            "datos"=>$o,
        ]);
    }
   
    public function actionProductos()
    {
        $p= Productos::find()
                ->all();
        
        return $this->render('productos',[
            "datos"=>$p,
        ]);
    }
    
    public function actionCategorias()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Categorias::find(),
        ]);
        
        return $this->render('categorias',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEstamos()
    {
        return $this->render('estamos');
    }
    
    public function actionSomos()
    {
        return $this->render('somos');
    }
    
    public function actionInformacion()
    {
        return $this->render('informacion');
    }
    
    public function actionContacto()
    {
        return $this->render('contacto');
    }
    
    public function actionDetalle($id){
        
        return $this->render("detalle",[
            "model"=>Categorias::find()->where(["id"=>$id])->one()
        ]);
    }
    
    
}
