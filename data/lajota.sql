﻿CREATE DATABASE IF NOT EXISTS jota;
USE jota;

CREATE OR REPLACE TABLE productos (
  id int AUTO_INCREMENT,
  nombre varchar(20),
  foto varchar(20),
  descripcion varchar(255),
  precio float,
  oferta boolean,
  PRIMARY KEY (id)
);

CREATE OR REPLACE TABLE categorias (
  id INT(11) NOT NULL AUTO_INCREMENT,
  foto VARCHAR(20),
  nombre VARCHAR(20),
  descripcion VARCHAR(100),
  PRIMARY KEY (id)
);

CREATE OR REPLACE TABLE relacion (
  id int AUTO_INCREMENT,
  categoria int,
  producto int,
  PRIMARY KEY (id)
  );

ALTER TABLE relacion
  ADD CONSTRAINT fk_relacion_categorias FOREIGN KEY (categoria)
    REFERENCES categorias (id);

ALTER TABLe relacion
  ADD CONSTRAINT fk_relacion_productos FOREIGN KEY (producto)
    REFERENCES productos (id);

ALTER TABLE relacion 
  ADD UNIQUE INDEX UK_relacion(categoria, producto);



INSERT INTO productos(id, nombre, foto, descripcion, precio, oferta) VALUES
(1, 'Conlifiedgor', 'foto1.jpg', 'Enim ullam fugiat sunt et dolore molestiae omnis nihil sit.', 432, 1),
(2, 'Projectry', 'foto2.jpg', 'Sed nam consequuntur.\r\nAccusantium ipsam ut. Soluta perspiciatis sed; doloribus voluptatem quam. Sed porro odio! Explicabo omnis vitae. Ut vel molestiae; delectus impedit ut. Sit consequatur autem.\r\n', 756, 1),
(3, 'Charcessridge', 'foto3.jpg', 'Eligendi obcaecati ab unde similique omnis voluptatem sit nisi ipsum.', 490, 1),
(4, 'Ancyclfiphone', 'foto4.jpg', 'Sit quia officiis iste, dolore sint et illum enim molestias. Facilis aliquam soluta unde expedita dignissimos quo quaerat quas officia.', 873, 0),
(5, 'Transcessletor', 'foto5.jpg', 'Impedit magnam incidunt a facere temporibus. Excepturi debitis perspiciatis. Quis et expedita. Neque sunt aspernatur. Placeat perspiciatis et; unde omnis dolorem. Quia quasi eius! Veniam rerum iste.', 931, 0),
(6, 'Confinder', 'foto6.jpg', 'Eius nostrum est architecto ullam recusandae numquam voluptas tempora laboriosam. Voluptatum laudantium sit error qui impedit voluptas nobis dolor. Debitis pariatur dolor vel fugiat laudantium!', 361, 1),
(7, 'Speakcessfiry', 'foto7.jpg', 'Quo dolorem alias. Officiis laboriosam tenetur! Sapiente voluptatem praesentium? Laboriosam excepturi omnis. Officia veritatis et.\r\nNisi incidunt debitis. Maiores facere dolor! Pariatur et officia.', 790, 1),
(8, 'Transbanderator', 'foto8.jpg', 'Ut praesentium dolor. Neque cumque explicabo! Quia facere nesciunt. Veniam possimus nihil. Nihil dolores non. Porro sit quia! Natus animi delectus. Vero cum omnis. Quibusdam explicabo ut. Omnis.\r\n', 762, 0),
(9, 'Transculgaentor', 'foto9.jpg', 'Et fugit ut.\r\nOfficiis quidem animi. Suscipit perspiciatis ut. Non ut inventore! Dolorem cupiditate quibusdam. Iste deserunt et. Laboriosam corporis unde.\r\nAut est totam. Sit nihil autem. Ut velit.', 63, 1),
(10, 'Playtinon', 'foto10.jpg', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 621, 1);

INSERT INTO categorias(id, foto, nombre, descripcion) VALUES
(1, 'categoria1.jpg', 'Libros', 'Quiere la boca exhausta vid, kiwi, piña y fugaz jamón.'),
(2, 'categoria2.jpg', 'Herramientas', 'Exhíbanse politiquillos zafios, con orejas kilométricas y uñas de gavilán.\r\n'),
(3, 'categoria3.jpg', 'Vehiculos', 'La cigüeña tocaba cada vez mejor el saxofón y el búho pedía kiwi y queso.;\r\n'),
(4, 'categoria4.jpg', 'Accesorios', 'El jefe buscó el éxtasis en un imprevisto baño de whisky y gozó como un duque.'),
(5, 'categoria5.jpg', 'Hogar', 'El pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y frío, añoraba.'),
(6, 'categoria6.jpg', 'Electronica', 'El cadáver de Wamba, rey godo de España, fue exhumado y trasladado en una caja de zinc que pesó un kilo.');


INSERT INTO relacion (categoria, producto)
  VALUES (1,10),(2,6),(2,9),(3,1),(3,2),(3,3),(4,1),(4,4),(4,7),(4,10),
  (5,5),(5,6),(5,8),(6,1),(6,4),(6,5),(6,7),(6,10);