<?php
    use yii\grid\GridView;
    use yii\helpers\Html;
?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [            
            'label' => 'Foto',
            'format' => 'raw',
            'value' => function($data){
                
                $url = \Yii::getAlias("@web/imgs/") . $data->foto;
                /*return Html::img($url , ['width' => '150px']);*/
                return Html::a(Html::img($url , ['width' => '150px']), ["site/detalle","id"=>$data->id]);
            },            
        ] ,
        'nombre',
        [
            'label' => 'Descripcion',
            'value' => function ($data){
                return substr($data->descripcion, 0, 5);
            }
        ]  
    ],
]);
?>