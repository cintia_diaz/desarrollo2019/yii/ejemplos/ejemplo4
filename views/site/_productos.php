<div class="col-sm-12 col-md-6 p-2 bg-primary">
    <div class="thumbnail">
        <div>
            <img src="<?= Yii::getAlias("@web") ."/imgs/" . $foto; ?>" 
                 class="center-block img-responsive">
        </div>
        <div class="caption">
            <h3><?= $nombre; ?></h3>
            <p><?= $descripcion; ?></p>
            <p><?= $precio; ?> €</p>
        </div>
    </div>
</div>
