<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Inicio - La Jota';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>¡Bienvenidos!</h1>

        <p class="lead">Ahora disfruta 24h de tu tienda online favorita.</p>

       
    </div>

    <div class="body-content">

        <div class="row">
            <?= 
                    Html::img("@web/imgs/tienda.png",[
                        "class" => "img-responsive img-circle center-block",
                        "alt" => "foto tienda"
                    ]);
            ?>           
        </div>
        <br>
        <div class="container">
        <div class="text-justify center-block">
            <p>Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia. 
            Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, 
            me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte. Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; 
            y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; 
            cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos 
            que penetran hasta el fondo del santuario, cuando recostado sobre la crecida hierba, cerca de la cascada, mi vista, más próxima a la tierra, 
            descubre multitud de menudas y diversas plantas; cuando siento más cerca de mi corazón los rumores de vida de ese pequeño mundo que
            palpita en los tallos de las hojas, y veo las formas innumerables e infinitas de los gusanillos y de los insectos; cuando siento, 
            en fin, la presencia del Todopoderoso, que nos ha creado</p>
            
        </div>
        </div>
        </div>

    </div>
</div>
