<div class="container">
    <div class="row row-flex row-flex-wrap">
        <?php
            $div='<div class="clearfix"></div>';
            foreach ($datos as $k=>$producto ){
                echo $this->render("_productos",[
                    "nombre"=>$producto->nombre,           
                    "descripcion"=>$producto->descripcion,
                    "precio"=>$producto->precio,
                    "foto"=>$producto->foto,
                    "reset"=>((($k+1)%3)==0 && $k!=0)?$div:"",
                ]);
            }
        ?>
    </div>
    
</div>
