<?php
use yii\helpers\Html;
use yii\widgets\DetailView;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'nombre',
        [
            'label' => 'Foto',
            'format' => 'html',
            'value' => function($model){
                
                $url = \Yii::getAlias("@web/imgs/") . $model->foto;
                return Html::img($url , ['width' => '150px','alt' => 'categoria']);
                
            },            
            
            $model->foto,
        ],
        'descripcion'
    ],
]);